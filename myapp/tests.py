from django.test import TestCase
from django.test import Client

class UnitTest(TestCase):
    def test_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_not_exist(self):
        response = Client().get('/oi')
        self.assertEqual(response.status_code, 404)

    def test_page(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

